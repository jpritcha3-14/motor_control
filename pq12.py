import serial
import time
import os
import re

def discover_serial_port():
    '''Examines the kernel ring buffer to discover the USB serial port for the
    acuator'''
    most_recent_port = None
    with os.popen('dmesg | grep -ie PL2303') as stream:
        for line in stream:
            attached = re.search(r'now attached to (\w+)', line)
            if attached is not None:
                most_recent_port = attached.group(1)
            detached = re.search(r'now disconnected from', line)
            if detached is not None:
                most_recent_port = None 
    if most_recent_port is None:
        raise(FileNotFoundError("Serial device not found"))
    return os.path.join('/dev', most_recent_port)

class Servo:
    _start = 255
    _commands = {'speed': 1,
                 'pos': 2    }

    def __init__(self, dev: str, channel: int, max_pos=1900, min_pos=1100,
            min_speed=1, max_speed=10):
        self.device = dev
        self.channel = channel
        self.max_pos = max_pos
        self.min_pos = min_pos
        self.max_speed = max_speed
        self.min_speed = min_speed
    
    def _constrain(self, val: int, min_val: int, max_val: int) -> int:
        '''Coerces a value to between a given min and max'''
        return max(min_val, min(max_val, val))

    def _get2bytes(self, decimal: int) -> (bytes, bytes):
        '''Gets the decimal representation of 2 bytes that compose a number'''
        return (decimal // 255, decimal % 255)
    
    def move(self, pos: int):
        '''
        Move the servo to a specified position

        Parameters:
            pos (int): Value between 0 and 100 for desired position
        '''

        port = serial.Serial(self.device)
        # convert range from 0 - 100 to 2000 to 1000, then constrian
        pos = self._constrain(2000 - (10 * pos), self.min_pos, self.max_pos)
        high_byte, low_byte = self._get2bytes(pos) 
        with port as s:
            s.write(bytearray((Servo._start, Servo._commands['pos'], self.channel, low_byte,
                high_byte)))
        print(bytearray((Servo._start, Servo._commands['pos'], self.channel, low_byte,
            high_byte)))

    def speed(self, speed: int):
        '''
        Set the speed of the servo

        Parameters:
            speed (int): Value between 1 and 10 for desired speed
        '''
        port = serial.Serial(self.device)
        speed = self._constrain(speed, self.min_speed, self.max_speed)
        high_byte, low_byte = self._get2bytes(speed) 
        with port as s:
            s.write(bytearray((Servo._start, Servo._commands['speed'], self.channel, low_byte,
                high_byte)))
        print(bytearray((Servo._start, Servo._commands['pos'], self.channel, low_byte,
            high_byte)))

if __name__ == '__main__':
    s = Servo(discover_serial_port(), 7)
    s.speed(2)
    s.move(100)
    time.sleep(10)
    s.move(0)
    time.sleep(10)
    s.speed(10)
    s.move(100)
    time.sleep(3)
    s.move(0)
