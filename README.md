# Introduction
This repo provides code for interfacing the actuonix PQ12 and L16 linear actuators through a serial connection.
## Dependencies
- pyusb
- pyserial
```
pip install pyusb
pip install pyserial
```
## Hardware Setup
### PQ12
The PQ12 Micro linear actuator is interfaced using the following hardware:

- EVISWIY PL2303TA USB to TTL Serial Cable

- WITMOTION 16 Channel PWM Servo Motor Driver Controller

- Any DC power supply capable of providing 6V / 1A

1. Connect the black (GND), green (TX), and white (RX) wires from PL2303TA  to the serial port on the motor driver board.  Note that the green (TX) wire should be connected to the RX connector on the port, and the white (RX) wire should be connected to the TX connector on the port.
**!!!IMPORTANT!!!**
**DO NOT connect the red wire** from the PL2303TA to the driver board.  This wire supplies 5V, while the controller board has a DC-to-DC converter that supplies 3.3V to the serial interface when it is powered by and external supply.  The PL2303TA also contains a DC-DC converter which drives the TX/RX wires with 3.3V.
2. Connect the DC power supply to the screw terminal block.
3. Connect the PQ12 to any port on the driver board (make sure the white signal wire connects to yellow pin on the port).

### PQ12 Wiring Diagram
![pq12 wiring diagram](readme_files/pq12_control_board_wiring.jpg)

## L16
The PQ12 Micro linear actuator is interfaced using the following hardware:

- Actuonix LAC control board

- USB A to USB Mini Type B cord

- Any DC power supply capable of providing 12V / 1A

1. Connect the L16 actuator to the pins shown below, making sure the yellow wire is closest to the corner of the board.
2. Connect the 12V power supply to the screw terminal block as shown below.
3. Connect the mini USB type B.

### L16 Wiring Diagram
![pq12 wiring diagram](readme_files/L16_lac_board_wiring.png)

## NOTE
There are two undocumented commands to read and write a custom parameter.  This can be used
to set distinct numbers for each LAC to identify them at startup.

- 0x50 -> Write custom 8-bit parameter 
- 0x51 -> Read custom 8-bit parameter

After writing the parameter, you must issue the 0x30 (disable defaults) command to
save the parameter to the EEPROM.  It will then persist between power cycles.

## Environment Setup

### PQ12

The PL2303TA USB to serial cable will show up as ttyUSBx in the /dev directory (Note that the servo controller board does not need to be powered for this device to appear).  ttyUSBx devices are most likely part of a group called 'dialout' or 'tty', which has read/write permissions to the devices.  Run the following command and logout and back in, or restart, to add your account to the dialout group.

```
sudo usermod -a -G dialout $USER
```

-usermod - modufy a user account  

-a - add the user to supplementary groups

G - a list of supplementary groups

dialout - group that control access to serial ports

$USER - bash variable containing current username

### L16

The LAC board is managed by the USB subsystem, and requires a new udev rule to allow non-root access.

Make sure the LAC board is powered with 12V from the power supply and run the following command.

```
lsusb > plugged
```

Now remove the 12V power and run the following commands

```
lsusb > unplugged
diff plugged unplugged
rm plugged unplugged
```

the diff commad should list the correct device information:

```
Bus 002 Device 009: ID 04d8:fc5f Microchip Technology, Inc.
```

Note the two hex numbers seperated by the colon, these are the vendor and device IDs for the board.  Yours may be different.  
 
```
sudo <editor of choice> /etc/udev/rules.d/50-descriptivename.rules
```

In your editor add the following to the new file (substituting your own vendor/product IDs if they are different) and save it:

```
SUBSYSTEM=="usb", ATTR{idVendor}=="04d8", ATTR{idProduct}=="fc5f", MODE="0666"
```

Finally reload and trigger the udev rules

```
sudo udevadm control --reload-rules && udevadm trigger
```
